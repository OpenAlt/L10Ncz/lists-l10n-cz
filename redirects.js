'use strict';

const MONTH_NAMES = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

let redirected = false;

function redirectTo(url) {
  if (!redirected) {
    redirected = true;
    window.location = url;
  }
}

const archivePathRegex = new RegExp('^/pipermail/diskuze/(?<year>[0-9]{4})-(?<monthName>.+)/', 'i');
const archivePathMatch = window.location.pathname.match(archivePathRegex);
const { year, monthName } = archivePathMatch?.groups ?? {};

try {
  if (year && monthName) {
    const monthIndex = MONTH_NAMES.indexOf(monthName.toLowerCase());
    if (monthIndex >= 0) {
      const date = new Date(year, monthIndex);
      const month = date.toLocaleDateString('en-US', { month: '2-digit' });
      redirectTo(`https://lists.openalt.org/wws/arc/diskuze-l10n-cz/${year}-${month}/`);
    }
  }
} catch(e) {
  console.error(e);
}

redirectTo('https://www.l10n.cz/emailova-konference/');
